extends Reference

class_name Command

var _name : String
var _args : Array
var _description : String = ""

func _init(name : String, args : Array, description : String):
	_name = name
	_args = args
	_description = description 

func set_name(name : String):
	_name = name
	
func set_args(args : Array):
	_args = args
	
func set_description(description : String):
	_description = description

func get_name() -> String:
	return _name
	
func get_args() -> Array:
	return _args
	
func get_description() -> String:
	return _description