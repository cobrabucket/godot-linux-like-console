"""
- contains a list of commands (Commands.gd)
"""
extends Control

signal on_message_sent
signal on_command_sent

const Command = preload("res://ChatBox/Command.gd")

var allText = ""
var messages = []
var currentIndex = -1
var resetSwitch = true
onready var lineEdit = $Panel/MarginContainer/Panel/MarginContainer/VBoxContainer/HBoxContainer/LineEdit
onready var textLabel = $Panel/MarginContainer/Panel/MarginContainer/VBoxContainer/RichTextLabel

const COMMAND_SIGN = '/'
var commands = []

var isShown = false

export(String) var next_message_history = "ui_down"
export(String) var previous_message_history = "ui_up"
export(String) var open_console = "open_console"

func _ready():
	hide()
	set_process_input(true)

func _input(event):
	if event.is_action_pressed(open_console):
		toggle_console()
	
	# left or right mouse button pressed
	# test if really needed
	if event.is_action_pressed("mouse_left") or event.is_action_pressed("mouse_right"):
		if event.position.x < get_position().x or event.position.y < get_position().y or event.position.x > get_position().x + get_size().x or event.position.y > get_position().y + get_size().y:
			lineEdit.focus_mode = FOCUS_NONE
		else:
			lineEdit.focus_mode = FOCUS_CLICK
	
	if event.is_action_pressed("ui_enter"):
		send_message(lineEdit.text)
	
	elif event.is_action_pressed(previous_message_history):
		if resetSwitch:
			messages.append(lineEdit.text)
			resetSwitch = false
		currentIndex -= 1
		if currentIndex < 0:
			currentIndex = messages.size() - 1
		lineEdit.text = messages[currentIndex]
		
	elif event.is_action_pressed(next_message_history):
		if resetSwitch:
			messages.append(lineEdit.text)
			resetSwitch = false
		currentIndex += 1
		if currentIndex > messages.size() - 1:
			currentIndex = 0
		lineEdit.text = messages[currentIndex]
		
	if event.is_action_pressed("ui_focus_next"):
		var closests = get_closest_commands(lineEdit.text)
		print(closests)
		if  closests != null:
			if closests.size() == 1:
				lineEdit.text = COMMAND_SIGN + closests[0]
				lineEdit.set_cursor_position(lineEdit.text.length())
			elif closests.size() > 1:
				var tempLine = lineEdit.text
				send_message_without_event("possible commands: ")
				for c in closests:
					send_message_without_event(COMMAND_SIGN + c, true)
					messages.append(COMMAND_SIGN + c)
				#send_message_without_event("Press [Up] or [Down] to cycle through available commands.", false)
				lineEdit.text = tempLine
				lineEdit.set_cursor_position(lineEdit.text.length())

func toggle_console() -> void:
	if isShown:
		hide()
	else:
		show()
		play_animation()
		
	isShown = !isShown

func get_last_message() -> String:
	return messages.back()
	

func play_animation() -> void:
	get_child(1).play("slide_in_chat")
	

func grab_line_focus() -> void:
	lineEdit.focus_mode = Control.FOCUS_ALL
	lineEdit.grab_focus()
	
	
func add_command(command : Command) -> void:
	commands.append(command)
	
func remove_command(commandName : String) -> bool:
	for i in range(commands.size()):
		if commands[i].get_name() == commandName:
			commands.remove(i)
			return true
	return false
	
	
func send_message(message):
	if message.empty():
		return
		
	if not resetSwitch:
		messages.pop_back()
	resetSwitch = true
	
	# let the message be switched through
	messages.append(message)
	currentIndex += 1
	
	allText += message
	
	# check if the input is a command
	if message[0] == COMMAND_SIGN:
		var currentCommand = message
		currentCommand.erase(0, COMMAND_SIGN)
		if is_input_real_command(currentCommand):
			# return the command and the whole message
			var cmd = get_command(currentCommand)
			if cmd == null:
				textLabel.add_text("Command not found!\n")
				return
			
			var found = false
			for i in range(commands.size()):
				if commands[i] == cmd:
					found = true
					emit_signal("on_command_entered", cmd, currentCommand)
					break
			if not found:
				textLabel.add_text("Commnd not found!\n")
		elif execute_if_system_command(currentCommand):
			lineEdit.text = ""
			return
		else:
			textLabel.add_text("Command not found!\n")
	else:
		textLabel.add_text(message)
		textLabel.newline()
		
	emit_signal("on_send", lineEdit.text)
	lineEdit.clear()


func execute_if_system_command(cmd):
	if cmd == "/clear":
		textLabel.clear()
		return true

	elif cmd == "/exit":
		hide()
		return true
		
	return false
	

func send_message_without_event(message, clickable = false):
	if message.empty():
		return
	
	if clickable:
		textLabel.push_meta("[u]" + message)
	
	allText += message
	textLabel.add_text(message)
	textLabel.newline()
	lineEdit.clear()
	
	if clickable:
		textLabel.pop()
	
	

# check first for real command
func get_command(command):
	if command[0] == COMMAND_SIGN:
		command.erase(0, COMMAND_SIGN)
	var regex = RegEx.new()
	# if command looks like: "/..."
	regex.compile("^" + COMMAND_SIGN + "(\\S+)\\s.*$")
	var result = regex.search(command)
	
	if result:
		command = result.get_string(1)
		for com in commands:
			if com == command:
				# commands[com] is the value
				return com
	

# before calling this method check for command sign
func is_input_real_command(command):
	if command.empty() or command[0] != COMMAND_SIGN:
		return false
		
	var regex = RegEx.new()
	regex.compile("^" + COMMAND_SIGN + "(\\S+).*$")
	var result = regex.search(command)
	
	command = result.get_string(1)
	
	if result:
		for com in commands:
			if com == command:
				# commands[com] is the value
				return true
	return false

func get_closest_commands(command):
	if command.empty() or command[0] != COMMAND_SIGN:
		return null
	
	var regex = RegEx.new()
	regex.compile("^" + COMMAND_SIGN + "(\\S+).*$")
	var result = regex.search(command)


	var results = []

	if result:
		command = result.get_string(1)
		for com in commands:
			if command in com:	
				# commands[com] is the value
				results.append(com)
		
		return results
		
	else:
		return null 

func _on_Button_pressed():
	send_message(lineEdit.text)
	lineEdit.grab_focus()
	

func _on_RichTextLabel_meta_clicked(meta):
	lineEdit.text = meta.substr(3, meta.length() - 3)
