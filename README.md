# Godot - Linux Like Console
- has auto complete (works like:  '/' + TAB) then it shows suggestions)
- shows commands that come kind of close to what you put in 
- comes with slide in animation
- 2 signals:
   -> on_send: every time message box receives message
   -> on_command_send: every time the message is a command
- command is defined by a starting string (default: /)
- /clear + /exit already implemented
