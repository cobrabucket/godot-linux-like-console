extends Node


onready var chat = $CommandLine

func _read():
	pass


remote func get_by_server(msg):
	chat.send_without_event_message(msg)
	

func _on_CommandLine_on_send(msg):
	if (msg[0] != '/'):
		rpc("get_by_server", msg)
