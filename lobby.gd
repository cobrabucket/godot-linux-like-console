extends Control

var MAX_PLAYERS = 2
var SERVER_PORT = 8796

func _on_CommandLine_on_send():
	
	pass

func _on_btn_exit_pressed():
	get_tree().quit()
	pass # Replace with function body.


func _on_btn_host_pressed():
	
	var host = NetworkedMultiplayerENet.new()
	# test compression / maybe no compression is better
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err = host.create_server(SERVER_PORT,MAX_PLAYERS) # max: 1 peer, since it's a 2 players game
	if (err!=OK):
		#is another server running?
		get_node("status").text = "unable to host"
		return
		
	get_tree().set_network_peer(host)
	get_node("btn_connect").set_disabled(true)
	get_node("btn_host").set_disabled(true)
	get_node("status").text = "hosting..."


func _on_btn_connect_pressed():
	var ip = get_node("lineEdit_address").get_text()
	if (not ip.is_valid_ip_address()):
		get_node("status").text = "invalid ip"
		return
	
	var host = NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	host.create_client(ip,SERVER_PORT)
	get_tree().set_network_peer(host)
	
	get_node("status").text = "connecting..."


func exit_game(msg=""):
	if (has_node("/root/Chat")):
		get_node("/root/Chat").free()
		show()
		
	get_tree().set_network_peer(null)
	get_node("btn_connect").set_disabled(true)
	get_node("btn_host").set_disabled(true)
	get_node("status").text = msg

# network related signals

func _player_connected(id):
	var chat = load("res://Chat.tscn").instance()
	
	get_tree().get_root().add_child(chat)
	hide()

func _player_disconnected(id):
	if (get_tree().is_network_server()):
		get_node("status").text = "Client disconnected"
	else:
		get_node("status").text = "Server disconnected"

func _connected_ok():
	pass

func _connected_fail():
	get_tree().set_network_peer(null)
	get_node("btn_connect").set_disabled(true)
	get_node("btn_host").set_disabled(true)
	get_node("status").text = "couldn't connect"

func _server_disconnected():
	exit_game("Server disconnected")

func _ready():
	get_tree().connect("network_peer_connected",self,"_player_connected")
	get_tree().connect("network_peer_disconnected",self,"_player_disconnected")
	get_tree().connect("connected_to_server",self,"_connected_ok")
	get_tree().connect("connection_failed",self,"_connected_fail")
	get_tree().connect("server_disconnected",self,"_server_disconnected")

